#!/usr/bin/env sh

source ./result_fnames.include

# variables:
variables="aws_vault:vs1 aws_region:eu-north-1 aws_access_key_id aws_secret_access_key ackyp_psize:8:should_be_a_power_of_2 ackyp_threads:8"

# creating and overwriting result files
(umask 077; echo -n > ${yaml_fname}; echo -n > ${mk_fname}; echo -n > ${pass_fname})

# interview
ask() {
    variable=(${1//:/ })
    read -p "${variable[0]} ${variable[2]} [${variable[1]}]: "
    printf -v "${variable[0]}" "%s" "${REPLY:-${variable[1]}}"
    echo "${variable[0]}: ${REPLY:-${variable[1]}}" >> ${yaml_fname}
    echo "${variable[0]}:=${REPLY:-${variable[1]}}" >> ${mk_fname}
}

for elem in $(echo ${variables})
do
    ask ${elem}
done

read -p "bcrypt password: "
echo -n "${REPLY}" >> ${pass_fname}

# supplement results
echo "ramdisk-size:=$((${ackyp_psize} * ${ackyp_threads}))G" >> ${mk_fname}

parent_dir_name=$(builtin cd ..; pwd)
echo "base_config_dir: ${parent_dir_name}" >> ${yaml_fname}
