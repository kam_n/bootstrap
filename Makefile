include result_fnames.include
-include $(mk_fname)

base_config_dir:=$(realpath $(dir ${PWD}))

dir-ackyp:=${base_config_dir}/.ackyp
dir-local-bin:=${HOME}/.local/bin
dir-aws:=${base_config_dir}/.aws

file-local-bin:=$(dir-local-bin)/ackyp
bcrypt-password:=$(base_config_dir)/.bcrypt_password
ramdisk:=/ackyp_ramdisk

templates:=$(shell find . -type f -name '*.mustache')
template-targets:=$(patsubst %.mustache,%,$(patsubst ./%,${base_config_dir}/%,$(templates)))

file-exists=$(info $(if $(wildcard $(1)),DA,HE): $(1))

check :
	@$(call file-exists,$(file-local-bin))
	@$(call file-exists,$(bcrypt-password))
	@$(foreach fl,$(template-targets),$(call file-exists,$(fl)))
	@$(call file-exists,$(dir-ackyp))
	@$(call file-exists,$(dir-aws))
	@$(info $(if $(shell findmnt $(ramdisk)),DA,HE): $(ramdisk))
	@ls -lU $(file-local-bin) $(bcrypt-password) $(template-targets) -ld $(dir-ackyp) $(dir-aws) $(ramdisk)
	@$(info Use 'all' to create all needed, 'remove-all' to remove all)

ifeq (,$(wildcard $(mk_fname)))
all :
	@$(info Run 'sh questionare.sh' before using 'all')
else
all : $(template-targets) $(file-local-bin) $(bcrypt-password) $(ramdisk)
endif

$(template-targets) :
	@(umask 077; \
		mkdir -p $(dir-ackyp) $(dir-aws); \
		cat $(yaml_fname) | mustache $(patsubst $(base_config_dir)/%,%,$@).mustache > $@)

$(file-local-bin) :
	@mkdir -p $(dir-local-bin)
	@gunzip -c ackyp.gz > $@
	@chmod 700 $@

$(bcrypt-password) :
	@mkdir -p $(dir-ackyp)
	@cp $(pass_fname) $@

.PHONY : $(ramdisk)
$(ramdisk) :
	@sudo mkdir -p $(ramdisk)
	@findmnt $(ramdisk) > /dev/null || sudo mount -o size=$(ramdisk-size) -t tmpfs none $@
	@sudo sh -c "chmod 700 $(ramdisk) && chown 1000:100 $(ramdisk)"

clean :
	@-rm $(mk_fname) $(pass_fname) $(yaml_fname)

remove-all : clean
	@-sudo umount $(ramdisk)
	@-sudo rmdir $(ramdisk)
	@-rm $(template-targets) $(bcrypt-password) $(file-local-bin)
	@-rmdir $(dir-local-bin) $(dir-ackyp) $(dir-aws)

remove :
	@-sudo umount $(ramdisk)
	@-sudo rmdir $(ramdisk)
	@-rm $(template-targets) $(bcrypt-password) $(file-local-bin)
	@-rmdir $(dir-local-bin) $(dir-ackyp) $(dir-aws)
